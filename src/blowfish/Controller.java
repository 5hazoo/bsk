package blowfish;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.util.Callback;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;


public class Controller implements Initializable {
    String PRIVPATH = "PrivKeys";

    private Main main;

    //potem usunac


    final ObservableList<User> listItems = FXCollections.observableArrayList();
    public ObservableList<User> listItemsDecrypt = FXCollections.observableArrayList();


    @FXML
    TextField textPlikWejsciowy;

    @FXML
    ChoiceBox<String> dlugoscKlucza;


    @FXML
    ChoiceBox<String> dlugoscPodbloku;

    @FXML
    ChoiceBox<String> dlugoscPodbloku2;

    @FXML
    ChoiceBox<String> tryb;

    @FXML
    TextField textPlikWyjsciowy;

    @FXML
    TextField textPlikWejsciowyDeszyfr;

    @FXML
    TextField textPlikWyjsciowyDeszyfr;

    @FXML
    ProgressBar progress;

    @FXML
    ProgressBar progress2;
    @FXML
    Label statusSztyf;

    @FXML
    Label statusOdsztyf;


    @FXML
    private ListView<User> ListaSzyfrowanie;

    @FXML
    private ListView<User> ListaDeszyfrowanie;

    @FXML
    PasswordField HasloField;

    File plikWejsciowy;
    File plikWejsciowyDeszyfr;

    File plikWyjsciowy;
    File plikWyjsciowyDeszyfr;
    File file;


    public void setMainApp(Main main) {
        this.main = main;
    }


    public void wybierzPlikOdczytu(ActionEvent e) {
        Node node = (Node) e.getSource();
        FileChooser chooser = new FileChooser();
        try {
            plikWejsciowy = chooser.showOpenDialog(node.getScene().getWindow());
            textPlikWejsciowy.setText(plikWejsciowy.toString());
            statusSztyf.setText("Wybrano plik");
        } catch (Exception s) {
            if (plikWejsciowy == null) {
                statusSztyf.setText("Nie wybrano pliku");

            } else {
                statusSztyf.setText("Plik nieprawidowy");
            }
        }

    }

    public void wybierzPlikOdczytuDeszyfr(ActionEvent e) {
        Node node = (Node) e.getSource();
        FileChooser chooser = new FileChooser();
        try {
            plikWejsciowyDeszyfr = chooser.showOpenDialog(node.getScene().getWindow());
            statusOdsztyf.setText("Wybrano plik");
            textPlikWejsciowyDeszyfr.setText(plikWejsciowyDeszyfr.toString());
        } catch (Exception s) {
            statusOdsztyf.setText("Nie wybrano pliku");

        }
        if (!plikWejsciowyDeszyfr.exists()) {
            statusOdsztyf.setText("Nie wybrano pliku");

        }
        File o = new File(plikWejsciowyDeszyfr.toString());
        listItemsDecrypt.clear();

        ObservableList<User> a = null;
        XMLParser parser = null;
        try {
            parser = new XMLParser(o);
            parser.getUser();
            a = parser.ret();
            for (User x : a)

            {
                listItemsDecrypt.add(x);
                System.out.print(x.Imie);
            }
        } catch (IOException e1) {
            e1.printStackTrace();
            statusOdsztyf.setText("Plik nieprawidlowy");

        }


    }

    public void deleteUser(ActionEvent e) {
        ObservableList<User> lu = FXCollections.observableArrayList();

//        try {
//            RSAEncryptionDescription.main();
//        } catch (IOException e1) {
//            e1.printStackTrace();
//        }
        System.out.print(lu.toString());

        int[] a = new int[10];
        ListaSzyfrowanie.getItems().clear();


    }


    public void szyfrujEvent(ActionEvent e) {
        ObservableList<User> listUser;

        progress.setProgress(0);
        listUser = ListaSzyfrowanie.getSelectionModel().getSelectedItems();

        if (plikWejsciowy != null && plikWyjsciowy != null) {
            Szyfrator szyf = new Szyfrator();
            String klucz = dlugoscKlucza.getSelectionModel().getSelectedItem();
            String podblok = dlugoscPodbloku.getSelectionModel().getSelectedItem();
            String mode = tryb.getSelectionModel().getSelectedItem();

            //System.out.print(menu.getSelectionModel().getSelectedItem());

            if ((mode == "CBC"|| mode =="ECB") && podblok == null) {
                podblok="0";
            }

            if ((mode != null && klucz != null && podblok != null) ) {
                if (!listUser.isEmpty()) {


                    statusSztyf.setText("Rozpoczeto szyfrowanie");

                    szyf.SzyfrowanieBlowfish(plikWejsciowy.toString(), plikWyjsciowy.toString(), listUser, mode, new Integer(podblok), new Integer(klucz));
                    progress.setProgress(1F);

                    statusSztyf.setText("Zakonczono");


//             szyf = new Szyfrator();
//            int i=0;
//            szyf.SzyfrowanieBlowfish(plikWejsciowy, plikWyjsciowy, listUser, i);
//            progress.setProgress(1F);
//}
                }
                else {
                    statusSztyf.setText("Brak odbiorcow");
                }
            } else {
                statusSztyf.setText("Nieprawidlowe parametry!");
            }


        } else {
            statusSztyf.setText("Nie wybrano pliku!");

        }
    }


    public void deszyfrujEvent(ActionEvent e) {

        Szyfrator szyf = new Szyfrator();
        progress2.setProgress(0);
        String t = HasloField.getText();
        if (plikWejsciowyDeszyfr != null && plikWyjsciowyDeszyfr != null) {

            User a = ListaDeszyfrowanie.getSelectionModel().getSelectedItem();
            for (User x : listItemsDecrypt)
            {
                System.out.print(x.Imie);
                System.out.print(a.Klucz);

                if (x.Imie.contains(a.Imie))
                {
                    a = x;
                }
            }
            System.out.print(a.Imie + " " + a.key + "\n");

            File f = new File(PRIVPATH + "//" + a.Imie + ".key");
            if(f.exists()) {

                if (a != null) {
                    if (!t.isEmpty()) {
                        try {
                            statusOdsztyf.setText("Rozpoczeto deszyfrowanie");
                            szyf.DeszyfrowanieBlowfish(plikWejsciowyDeszyfr.toString(), plikWyjsciowyDeszyfr.toString(), a, t);
                            statusOdsztyf.setText("Odszyfrowano");

                            progress2.setProgress(1F);

                        } catch (NoSuchAlgorithmException e1) {
                        }
                    } else {
                        statusOdsztyf.setText("Nie wpisano hasla");
                    }
                } else {
                    statusOdsztyf.setText("Nie wybrano uzytkownika");
                }
            }
            else {
                statusOdsztyf.setText("Brak uzytkownika");
            }
        } else {
            statusOdsztyf.setText("Nie wybrano pliku");
        }


    }

    public void wybierzPlikZapisu(ActionEvent e) {
        Node node = (Node) e.getSource();
        FileChooser chooser = new FileChooser();
        plikWyjsciowy = chooser.showSaveDialog(node.getScene().getWindow());
        textPlikWyjsciowy.setText(plikWyjsciowy.toString());
    }

    public void wybierzPlikZapisuDeszyfr(ActionEvent e) {
        Node node = (Node) e.getSource();
        FileChooser chooser = new FileChooser();
        plikWyjsciowyDeszyfr = chooser.showSaveDialog(node.getScene().getWindow());
        textPlikWyjsciowyDeszyfr.setText(plikWyjsciowyDeszyfr.toString());
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ListaSzyfrowanie.setItems(listItems);
        ListaDeszyfrowanie.setItems(listItemsDecrypt);

        ListaSzyfrowanie.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

//ECB, CBC, CFb, OFB
// ofb i cfb podbloki 8, do dlugosci calego bloku, wielokrotnosci 8
//ja mia�em 128, 256 i 448 d�ugo�ci klucza. Spojrza� i zaakceptowa�.
// D�ugo�ci podbloku mia�em 8, 16, 32, ale na to nie patrzy�.
// Widzia�em w starych sprawkach, �e chyba takie warto�ci powinny by�, cho� nie jestem pewien, czy nie powinna by� te� d�ugos� bloku, czyli 64
        final ObservableList<String> items = FXCollections.observableArrayList();
        items.add("ECB");
        items.add("CBC");
        items.add("CFB");
        items.add("OFB");
        tryb.setItems(items);
        ObservableList<String> items2 = FXCollections.observableArrayList();

        items2.add("128");
        items2.add("256");
        items2.add("448");
        //  items2.add("OFB");
        dlugoscKlucza.setItems(items2);

        ObservableList<String> items3 = FXCollections.observableArrayList();

        items3.add("8");
        items3.add("16");
        items3.add("32");
        //items3.add("OFB");
        dlugoscPodbloku.setItems(items3);

        dlugoscPodbloku.showingProperty();


        tryb.getSelectionModel().selectedIndexProperty().addListener(
                new ChangeListener<Number>() {
                    @Override
                    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                        if (items.get(newValue.intValue()).equals("CBC") || items.get(newValue.intValue()).equals("ECB")) {
                            System.out.print("sel");

                            dlugoscPodbloku.setVisible(false);
                            dlugoscPodbloku2.setVisible(true);

                        }
                        else {
                            dlugoscPodbloku.setVisible(true);
                            dlugoscPodbloku2.setVisible(false);

                        }
                    }
                }
        );
        ListaSzyfrowanie.setCellFactory(new Callback<ListView<User>, ListCell<User>>() {

            @Override
            public ListCell<User> call(ListView<User> p) {

                ListCell<User> cell = new ListCell<User>() {

                    @Override
                    protected void updateItem(User t, boolean bln) {
                        super.updateItem(t, bln);
                        if (t != null) {
                            setText(t.Imie);
                        }
                    }

                };

                return cell;
            }
        });
        ListaDeszyfrowanie.setCellFactory(new Callback<ListView<User>, ListCell<User>>() {

            @Override
            public ListCell<User> call(ListView<User> p) {

                ListCell<User> cell = new ListCell<User>() {

                    @Override
                    protected void updateItem(User t, boolean bln) {
                        super.updateItem(t, bln);
                        if (t != null) {
                            setText(t.Imie);
                        }
                    }

                };

                return cell;
            }
        });

    }

    public void openPopup(ActionEvent e) throws Exception {
        Node node = (Node) e.getSource();
        FileChooser chooser = new FileChooser();
        File file;
        User a = new User();
        file = chooser.showOpenDialog(node.getScene().getWindow());
        if (file != null) {
            a.Klucz = file.toString();
            a.Imie = file.getName();
            listItems.add(a);
        }


//        a.Klucz = key.getText();
//        listItems.add(a);


//        final Node node = (Node) e.getSource();
//        Dialog<Pair<String, String>> dialog = new Dialog();
//        dialog.setHeaderText("Podaj Nazw� uzytkownika");
//
//// Set the icon (must be included in the project).
//
//// Set the button types.
//        ButtonType loginButtonType = new ButtonType("Dodaj", ButtonBar.ButtonData.OK_DONE);
//        dialog.getDialogPane().getButtonTypes().addAll(loginButtonType, ButtonType.CANCEL);
//
//// Create the username and password labels and fields.
//        GridPane grid = new GridPane();
//        grid.setHgap(10);
//        grid.setVgap(10);
//        grid.setPadding(new Insets(20, 150, 10, 10));
//
//        final TextField username = new TextField();
//        final TextField key = new TextField();
//        Button ad = new Button("Dodaj");
//        ad.setOnAction(
//                new EventHandler<ActionEvent>() {
//                    @Override
//                    public void handle(final ActionEvent e) {
//                        FileChooser chooser = new FileChooser();
//                        file = chooser.showOpenDialog(node.getScene().getWindow());
//                        key.setText(file.toString());
//
//
//                    }
//                });
//
//
//        grid.add(new Label("Imie:"), 0, 0);
//        grid.add(username, 1, 0);
//        grid.add(new Label("Klucz:"), 0, 1);
//        grid.add(key, 1, 1);
//        grid.add(ad, 2, 1);
//
//// Enable/Disable login button depending on whether a username was entered.
//        Node loginButton = dialog.getDialogPane().lookupButton(loginButtonType);
//        loginButton.setDisable(true);
//
//// Do some validation (using the Java 8 lambd
//
//        dialog.getDialogPane().setContent(grid);
//
//        dialog.getDialogPane().setContent(grid);
//        if (key!=null && username!=null){
//            loginButton.setDisable(false);
//
//        }
//        Optional<Pair<String, String>> result = dialog.showAndWait();
//
//
//        final User a = new User();
//
//        Collection r = CollectionUtils.select(listItems, new Predicate() {
//            public boolean evaluate(Object b) {
//                return ((User) b).Imie == username.getText();
//            }
//        });
//        System.out.print(r+ "213");
//        a.Imie = username.getText();
//        a.Klucz = key.getText();
//        listItems.add(a);
    }


}
