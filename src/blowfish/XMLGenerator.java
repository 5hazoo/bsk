package blowfish;

/**
 * Created by Marcin on 2015-04-22.
 */

import javafx.collections.ObservableList;
import javax.xml.bind.DatatypeConverter;

import java.io.*;

public class XMLGenerator {
    private String name = "";
    private String mode;
    private int subblock;
    private int keysize;
    private byte[] key;
    private byte[] data;
    private ObservableList<User> users;
    private byte[] iv;


    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public void setKeySize(int keysize) {
        this.keysize = keysize;
    }


    public int getSubblock() {
        return subblock;
    }

    public void setSubblock(int size) {
        this.subblock = size;
    }

    public ObservableList<User> getUsers() {
        return users;
    }

    public void setUsers(ObservableList<User> users) {
        this.users = users;
    }

    public byte[] getKey() {
        return key;
    }



    public void setIv( byte[] iv) {
        this.iv = iv;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
    public void setKey (byte [] key) {
        this.key = key;
    }

    public void generateUser(ObservableList<User> list) {
        this.users = list;
    }

    public void generate(File file) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            fos.write(("<?xml version='1.0' encoding='UTF-8'?>" + '\n' + "<EncryptedFileHeader>" + '\n' + "<Algorithm>Blowfish</Algorithm>" + '\n').getBytes());

            fos.write(("<CipherMode>" + mode + "</CipherMode>" + '\n').getBytes());

            fos.write(("<SubblockSize>" + subblock + "</SubblockSize>" + '\n').getBytes());
            fos.write(("<KeySize>" + keysize + "</KeySize>" + '\n').getBytes());

            fos.write("<iv>".getBytes());
            fos.write(DatatypeConverter.printHexBinary(iv).getBytes());
            fos.write(("</iv>" + '\n').getBytes());


            fos.write(("<ApprovedUsers>" + '\n').getBytes());
//            fos.write(("<SessionKey>").getBytes());
//                fos.write((key));
//                fos.write(("</SessionKey>" + '\n').getBytes());
            for (User x : users) {
                fos.write(("<User>" + '\n').getBytes());
                fos.write(("<Name>").getBytes());
                fos.write((x.Imie).getBytes());
                fos.write(("</Name>" + '\n').getBytes());
                fos.write(("<SessionKey>").getBytes());
                fos.write(DatatypeConverter.printHexBinary(x.key).getBytes());
                fos.write(("</SessionKey>" + '\n').getBytes());

                fos.write(("</User>" + '\n').getBytes());

            }
            fos.write(("</ApprovedUsers>" + '\n').getBytes());
            fos.write(("</EncryptedFileHeader>").getBytes());
            fos.write(data);

            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}