package blowfish;

import javafx.collections.ObservableList;
import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.util.PublicKeyFactory;
import sun.misc.BASE64Decoder;

import java.io.*;
import java.security.Security;

/**
 * Created by Marcin on 2015-04-22.
 */
public class Utils {


//    private void encrypt (String publicKeyFilename, String inputFilename, String encryptedFilename){
//
//        try {
//
//            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
//
//            String key = readFileAsString(publicKeyFilename);
//            BASE64Decoder b64 = new BASE64Decoder();
//            AsymmetricKeyParameter publicKey =
//                    (AsymmetricKeyParameter) PublicKeyFactory.createKey(b64.decodeBuffer(key));
//            AsymmetricBlockCipher e = new RSAEngine();
//            e = new org.bouncycastle.crypto.encodings.PKCS1Encoding(e);
//            e.init(true, publicKey);
//
//            String inputdata = readFileAsString(inputFilename);
//            byte[] messageBytes = inputdata.getBytes();
//            byte[] hexEncodedCipher = e.processBlock(messageBytes, 0, messageBytes.length);
//
//            System.out.println(getHexString(hexEncodedCipher));
//            BufferedWriter out = new BufferedWriter(new FileWriter(encryptedFilename));
//            out.write(getHexString(hexEncodedCipher));
//            out.close();
//
//        }
//        catch (Exception e) {
//            System.out.println(e + "chuj");
//        }
//    }

    private String encrypt (String publicKeyFilename, String inputData, ObservableList<User> userList, int tryb){

        String encryptedData = null;
        try {

            Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());

            BASE64Decoder b64 = new BASE64Decoder();
            String key = readFileAsString(publicKeyFilename);
            AsymmetricKeyParameter publicKey =
                    (AsymmetricKeyParameter) PublicKeyFactory.createKey(b64.decodeBuffer(key));
            AsymmetricBlockCipher e = new RSAEngine();
            e = new org.bouncycastle.crypto.encodings.PKCS1Encoding(e);
            e.init(true, publicKey);

            byte[] messageBytes = inputData.getBytes();
            byte[] hexEncodedCipher = e.processBlock(messageBytes, 0, messageBytes.length);

            System.out.println(getHexString(hexEncodedCipher));
            encryptedData = getHexString(hexEncodedCipher);

        }
        catch (Exception e) {
            System.out.println(e);
        }

        return encryptedData;
    }

    public static String getHexString(byte[] b) throws Exception {
        String result = "";
        for (int i=0; i < b.length; i++) {
            result +=
                    Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 );
        }
        return result;
    }

    private static String readFileAsString(String filePath)
            throws java.io.IOException{
        StringBuffer fileData = new StringBuffer(1000);
        BufferedReader reader = new BufferedReader(
                new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead=0;
        while((numRead=reader.read(buf)) != -1){
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
            buf = new char[1024];
        }
        reader.close();
        System.out.println(fileData.toString());
        return fileData.toString();
    }
    public static String binary_to_hex(String binary) {
        String hex = "";
        String hex_char;
        int len = binary.length()/8;
        for(int i=0;i<len;i++){
            String bin_char = binary.substring(8*i,8*i+8);
            int conv_int = Integer.parseInt(bin_char,2);
            hex_char = Integer.toHexString(conv_int);
            if(i==0) hex = hex_char;
            else hex = hex+hex_char;
        }
        return hex;
    }
    public static String hex_to_binary(String hex) {
        String hex_char,bin_char,binary;
        binary = "";
        int len = hex.length()/2;
        for(int i=0;i<len;i++){
            hex_char = hex.substring(2*i,2*i+2);
            int conv_int = Integer.parseInt(hex_char,16);
            bin_char = Integer.toBinaryString(conv_int);
            bin_char = zero_pad_bin_char(bin_char);
            if(i==0) binary = bin_char;
            else binary = binary+bin_char;
            //out.printf("%s %s\n", hex_char,bin_char);
        }
        return binary;
    }
    public static String zero_pad_bin_char(String bin_char){
        int len = bin_char.length();
        if(len == 8) return bin_char;
        String zero_pad = "0";
        for(int i=1;i<8-len;i++) zero_pad = zero_pad + "0";
        return zero_pad + bin_char;
    }
}
