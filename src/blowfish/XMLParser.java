package blowfish;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.util.Pair;

import javax.xml.bind.DatatypeConverter;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class XMLParser {
    private File file;
    private ObservableList<User> users;

    XMLParser(File file) throws IOException {
        this.file = file;
        this.users = FXCollections.observableArrayList();
//        setUsers();
    }

    public byte[] getKey() throws IOException {
//        int beginIndex = getIndex(userName);
        return DatatypeConverter.parseHexBinary(new String(getValue("<SessionKey>", "</SessionKey>", 0)));
    }
    public String getKeySize() throws IOException {
//        int beginIndex = getIndex(userName);
        return new String(getValue("<KeySize>", "</KeySize>", 0));
    }


    public byte[] getModulus() throws IOException {
        return DatatypeConverter.parseHexBinary(new String(getValue("<Modulus>", "</Modulus>", 0)));
    }

    public byte[] getExponent() throws IOException {
        return getValue("<Exponent>", "</Exponent>", 0);
    }

    public String getMode() throws IOException {
        return new String(getValue("<CipherMode>", "</CipherMode>", 0));
    }

    public String getSubBlockLength() throws IOException {
        return new String(getValue("<SubblockSize>", "</SubblockSize>", 0));
    }

    public byte[] getIv() throws IOException {
        return DatatypeConverter.parseHexBinary(new String(getValue("<iv>", "</iv>", 0)));
    }

    //    public byte[] getKey() throws IOException{
//        return getValue("<SessionKey>", "</SessionKey>", 0);
//    }
//    public byte[] getData() throws IOException {
//        return getValue("<data>", "</data>", 0);
//
//    }

    public byte[] getData() throws IOException {
        byte[] data = getDataFromFile();
        int start = indexOf(data, "</EncryptedFileHeader>".getBytes(), 0) + "</EncryptedFileHeader>".getBytes().length;
        int end =data.length;

        return Arrays.copyOfRange(data, start, end);
    }

    public int getBeginOfData() throws IOException {
        byte[] data = getDataFromFile();
        return indexOf(data, "</encryptData>\n".getBytes(), 0) + "</encryptData>\n".getBytes().length;
    }

    public ObservableList<User> getUsers() {
        return users;
    }

    private byte[] getName(int beginIndex) throws IOException {
        return getValue("<name>", "</name>", beginIndex);
    }

//    private int getIndex(String name){
//        for(int i = 0; i < users.size(); i++) {
//            if (users.get(i).equals(name)) {
//                return users.get(i).getValue();
//            }
//        }
//        return -1;
//    }
//
//    private void setUsers() throws IOException{
//        int beginIndex = 0;
//        byte[] data = getDataFromFile();
//        while(true) {
//            beginIndex = indexOf(data, "<user>".getBytes(), beginIndex) + "<user>".getBytes().length;
//            if (beginIndex - 7 > 0) {
//                String name = new String(getName(beginIndex));
//                User a = new User();
//                a.Imie = imie;
//                Pair<String, Integer> pair = new Pair<String, Integer>(name, beginIndex);
//                users.add(pair);
//            }
//            else break;
//        }
//    }


    public void getUser() throws IOException {
        byte[] a = getValue("<ApprovedUsers>", "</ApprovedUsers>", 0);
        byte[] b = new byte[0];
        while (a != null) {
            try {
                b = getValueFromData(a, "<User>", "</User>", 0);
                User u = new User();
                u.Imie = new String(getValueFromData(b, "<Name>", "</Name>", 0));
                u.key = DatatypeConverter.parseHexBinary(new String(getValueFromData(b, "<SessionKey>", "</SessionKey>", 0)));
                System.out.print(u.Imie);
                a = getValueRest(a, "<User>", "</User>", 0);
                users.add(u);
            } catch (Exception e) {
                break;
            }
        }


    }
    public ObservableList<User> ret() throws IOException {
        return this.users;
    }

    public byte[] getValue(String beginTag, String endTag, int beginIndex) throws IOException {
        byte[] data = getDataFromFile();
        int start = indexOf(data, beginTag.getBytes(), beginIndex) + beginTag.getBytes().length;
        int end = indexOf(data, endTag.getBytes(), beginIndex);

        return Arrays.copyOfRange(data, start, end);
    }

    public byte[] getValueFromData(byte[] data, String beginTag, String endTag, int beginIndex) throws IOException {
        int start = indexOf(data, beginTag.getBytes(), beginIndex) + beginTag.getBytes().length;
        int end = indexOf(data, endTag.getBytes(), beginIndex);
        return Arrays.copyOfRange(data, start, end);

    }

    public byte[] getValueRest(byte[] data, String beginTag, String endTag, int beginIndex) throws IOException {
        int start = indexOf(data, beginTag.getBytes(), beginIndex) + beginTag.getBytes().length;
        int end = indexOf(data, endTag.getBytes(), beginIndex) + endTag.getBytes().length;
        System.out.print(start + " " + end + '\n');
        return Arrays.copyOfRange(data, end, data.length);
    }


    private byte[] getDataFromFile() throws FileNotFoundException, IOException {
        FileInputStream fis = new FileInputStream(file.getPath());
        Path filePath = Paths.get(file.toString());
        byte[] buffer = Files.readAllBytes(filePath);
        return buffer;
    }

    //Algorytm Knutha-Morrisa-Pratta
    private int indexOf(byte[] data, byte[] pattern, int beginByte) {
        int[] failure = computeFailure(pattern);
        int j = 0;

        for (int i = beginByte; i < data.length; i++) {
            while (j > 0 && pattern[j] != data[i]) {
                j = failure[j - 1];
            }
            if (pattern[j] == data[i]) {
                j++;
            }
            if (j == pattern.length) {
                return i - pattern.length + 1;
            }
        }
        return -1;
    }

    private int[] computeFailure(byte[] pattern) {
        int[] failure = new int[pattern.length];
        int j = 0;

        for (int i = 1; i < pattern.length; i++) {
            while (j > 0 && pattern[j] != pattern[i]) {
                j = failure[j - 1];
            }
            if (pattern[j] == pattern[i]) {
                j++;
            }
            failure[i] = j;
        }
        return failure;
    }
}
