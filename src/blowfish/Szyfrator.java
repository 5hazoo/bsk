package blowfish;


import javafx.collections.ObservableList;
import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.engines.BlowfishEngine;
import org.bouncycastle.crypto.modes.CBCBlockCipher;
import org.bouncycastle.crypto.modes.CFBBlockCipher;
import org.bouncycastle.crypto.modes.OFBBlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.paddings.ZeroBytePadding;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.crypto.params.ParametersWithIV;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

/**
 * Created by Marcin on 2015-04-18.
 */
public class Szyfrator {
    String PRIVPATH = "PrivKeys";
    String PUBPATH = "PubKeys";


    public void SzyfrowanieBlowfish(String plikWejsciowy, String plikWyjsciowy, ObservableList<User> userList, String tryb, int podblok, int dlugoscKlucza) {

        // public void SzyfrowanieBlowfish(File plikWejsciowy, File plikWyjsciowy , ObservableList<User> userList, int tryb) {
        byte[] keyBytes;

        byte[] iv;
        Path filePath = Paths.get(plikWejsciowy);

        //   Path filePath = Paths.get(plikWejsciowy.getPath());
        byte[] message2 = new byte[0];
        try {
            message2 = Files.readAllBytes(filePath);
        } catch (IOException e) {
            e.printStackTrace();
        }

        keyBytes = new byte[dlugoscKlucza / 8];

        new Random(System.nanoTime()).nextBytes(keyBytes);

        iv = new byte[8];
        new Random(System.nanoTime()).nextBytes(iv);


        BlowfishEngine blowFishEng = new BlowfishEngine();

        KeyParameter kpar = new KeyParameter(keyBytes);
        ParametersWithIV parIV = new ParametersWithIV(kpar, iv);

//ECB, CBC, CFb, OFB
        byte[] cipherText2;
        if (tryb.equals("CBC")) {
            CBCBlockCipher cbcrc6 = new CBCBlockCipher(blowFishEng);
            PaddedBufferedBlockCipher paddcbc = new PaddedBufferedBlockCipher(cbcrc6);
            paddcbc.init(true, parIV);
            cipherText2 = new byte[paddcbc.getOutputSize(message2.length)];
            System.out.println("Output size= " + paddcbc.getOutputSize(message2.length));
            int nprocess2 = paddcbc.processBytes(message2, 0, message2.length, cipherText2, 0);
            try {
                nprocess2 = paddcbc.doFinal(cipherText2, nprocess2);
            } catch (DataLengthException ex) {
                ex.printStackTrace();
            } catch (InvalidCipherTextException ex) {
                ex.printStackTrace();
            } catch (IllegalStateException ex) {
                ex.printStackTrace();
            }

        } else if (tryb.equals("CFB")) {
            CFBBlockCipher cbcrc6 = new CFBBlockCipher(blowFishEng, podblok);
            PaddedBufferedBlockCipher paddcbc = new PaddedBufferedBlockCipher(cbcrc6);
            paddcbc.init(true, parIV);
            cipherText2 = new byte[paddcbc.getOutputSize(message2.length)];
            System.out.println("Output size= " + paddcbc.getOutputSize(message2.length));
            int nprocess2 = paddcbc.processBytes(message2, 0, message2.length, cipherText2, 0);
            try {
                nprocess2 = paddcbc.doFinal(cipherText2, nprocess2);
            } catch (DataLengthException ex) {
                ex.printStackTrace();
            } catch (InvalidCipherTextException ex) {
                ex.printStackTrace();
            } catch (IllegalStateException ex) {
                ex.printStackTrace();
            }

        } else if (tryb.equals("OFB")) {
            OFBBlockCipher cbcrc6 = new OFBBlockCipher(blowFishEng, podblok);
            PaddedBufferedBlockCipher paddcbc = new PaddedBufferedBlockCipher(cbcrc6);
            paddcbc.init(true, parIV);
            cipherText2 = new byte[paddcbc.getOutputSize(message2.length)];
            System.out.println("Output size= " + paddcbc.getOutputSize(message2.length));
            int nprocess2 = paddcbc.processBytes(message2, 0, message2.length, cipherText2, 0);
            try {
                nprocess2 = paddcbc.doFinal(cipherText2, nprocess2);
            } catch (DataLengthException ex) {
                ex.printStackTrace();
            } catch (InvalidCipherTextException ex) {
                ex.printStackTrace();
            } catch (IllegalStateException ex) {
                ex.printStackTrace();
            }

        } else {
            //ECB?
            // OFBBlockCipher cbcrc6 = new OFBBlockCipher(blowFishEng,podblok);
            PaddedBufferedBlockCipher paddcbc = new PaddedBufferedBlockCipher(blowFishEng);
            paddcbc.init(true, kpar);
            cipherText2 = new byte[paddcbc.getOutputSize(message2.length)];
            System.out.println("Output size= " + paddcbc.getOutputSize(message2.length));
            int nprocess2 = paddcbc.processBytes(message2, 0, message2.length, cipherText2, 0);
            try {
                nprocess2 = paddcbc.doFinal(cipherText2, nprocess2);
            } catch (DataLengthException ex) {
                ex.printStackTrace();
            } catch (InvalidCipherTextException ex) {
                ex.printStackTrace();
            } catch (IllegalStateException ex) {
                ex.printStackTrace();
            }

        }


        FileOutputStream out;

        XMLGenerator generator = new XMLGenerator();
        generator.setData(cipherText2);
        generator.setSubblock(podblok);
        generator.setIv(iv);
        generator.setMode(tryb);
        generator.setKeySize(dlugoscKlucza);
        for (User x : userList) {
            try {
                x.key = RSAEncryptionDescription.encryptData(keyBytes, PUBPATH + "//" + x.Imie);
                System.out.print(x.Imie + keyBytes.toString() + "\n");
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        generator.setUsers(userList);

        File file = new File(plikWyjsciowy);
        generator.generate(file);

        //  try {
        // out = new FileOutputStream(plikWyjsciowy);
        //   out.write(cipherText2);

//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e
// .printStackTrace();
//        }

    }

    public void DeszyfrowanieBlowfish(String plikWejsciowy, String plikWyjsciowy, User a, String pass) throws NoSuchAlgorithmException {
        //   Path filePath = Paths.get(plikWejsciowy));
        byte[] message = new byte[0];
        byte[] iv = new byte[8];
        String mode = new String();
        int subblock = 0;
        byte[] keyBytes = new byte[0];
        byte[] k = new byte[0];
        byte[] plainText = new byte[0];
        boolean czyIstnieje = false;
        int keysize = 0;

        File folder = new File(PRIVPATH);
        File[] listOfFiles = folder.listFiles();
        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile() && listOfFiles[i].getName().equals(a.Imie + ".key")) {
                czyIstnieje = true;
                System.out.println("File " + listOfFiles[i].getName());

            }
        }


        if (czyIstnieje) {
            File file = new File(plikWejsciowy);
            try {
                XMLParser parser = new XMLParser(file);
//            parser.getUser();


                //asdasd
                message = parser.getData();
//            keyBytes = parser.getKey();
                iv = parser.getIv();
                System.out.print(iv);
                // k = parser.getKey();
                mode = parser.getMode();
                keysize = new Integer(parser.getKeySize());
                System.out.print(keysize);
                keysize = new Integer(parser.getKeySize());
                System.out.print("ks " + keysize);
                subblock = new Integer(parser.getSubBlockLength());
//            parser.getUser();

            } catch (IOException e) {
                e.printStackTrace();
            }

            KeyParameter kpar;
            boolean nope = false;
            try {

                keyBytes = RSAEncryptionDescription.decryptDataS(a.key, PRIVPATH + "//" + a.Imie + ".key", pass);
                kpar = new KeyParameter(keyBytes);

            } catch (Exception e) {
                nope = true;
                byte[] ke = new byte[keysize / 8];
                for (int i = 0; i < keysize / 8; i++) {
                    ke[i] = 0;
                }

                kpar = new KeyParameter(ke);
            }


            ParametersWithIV parIV = new ParametersWithIV(kpar, iv);
            BlowfishEngine blowFishEng = new BlowfishEngine();
            if (!nope) {
                if (mode.equals("CBC")) {
                    CBCBlockCipher cbcrc6 = new CBCBlockCipher(blowFishEng);
                    PaddedBufferedBlockCipher paddcbc = new PaddedBufferedBlockCipher(cbcrc6);
                    paddcbc.reset();
                    paddcbc.init(false, parIV);
                    // decryption pass
                    plainText = new byte[paddcbc.getOutputSize(message.length)];
                    int ptLength = paddcbc.processBytes(message, 0, message.length, plainText, 0);
                    try {
                        ptLength += paddcbc.doFinal(plainText, ptLength);
                    } catch (Exception ex) {
                    }

                } else if (mode.equals("CFB")) {
                    CFBBlockCipher cbcrc6 = new CFBBlockCipher(blowFishEng, subblock);
                    PaddedBufferedBlockCipher paddcbc = new PaddedBufferedBlockCipher(cbcrc6);
                    paddcbc.reset();
                    paddcbc.init(false, parIV);
                    // decryption pass
                    plainText = new byte[paddcbc.getOutputSize(message.length)];
                    int ptLength = paddcbc.processBytes(message, 0, message.length, plainText, 0);
                    try {
                        ptLength += paddcbc.doFinal(plainText, ptLength);
                    } catch (Exception ex) {

                    }

                } else if (mode.equals("OFB")) {
                    OFBBlockCipher cbcrc6 = new OFBBlockCipher(blowFishEng, subblock);
                    PaddedBufferedBlockCipher paddcbc = new PaddedBufferedBlockCipher(cbcrc6);
                    paddcbc.reset();
                    paddcbc.init(false, parIV);
                    // decryption pass
                    plainText = new byte[paddcbc.getOutputSize(message.length)];
                    int ptLength = paddcbc.processBytes(message, 0, message.length, plainText, 0);
                    try {
                        ptLength += paddcbc.doFinal(plainText, ptLength);
                    } catch (Exception ex) {
                    }

                } else {
                    PaddedBufferedBlockCipher paddcbc = new PaddedBufferedBlockCipher(blowFishEng);
                    paddcbc.reset();
                    paddcbc.init(false, kpar);
                    // decryption pass
                    plainText = new byte[paddcbc.getOutputSize(message.length)];
                    int ptLength = paddcbc.processBytes(message, 0, message.length, plainText, 0);
                    try {
                        ptLength += paddcbc.doFinal(plainText, ptLength);
                    } catch (Exception ex) {
                    }
                }
            } else {
                if (mode.equals("CBC")) {
                    CBCBlockCipher cbcrc6 = new CBCBlockCipher(blowFishEng);
                    PaddedBufferedBlockCipher paddcbc = new PaddedBufferedBlockCipher(cbcrc6, new ZeroBytePadding());
                    paddcbc.reset();
                    paddcbc.init(false, parIV);
                    // decryption pass
                    plainText = new byte[paddcbc.getOutputSize(message.length)];
                    int ptLength = paddcbc.processBytes(message, 0, message.length, plainText, 0);
                    try {
                        ptLength += paddcbc.doFinal(plainText, ptLength);
                    } catch (Exception ex) {
                    }

                } else if (mode.equals("CFB")) {
                    CFBBlockCipher cbcrc6 = new CFBBlockCipher(blowFishEng, subblock);
                    PaddedBufferedBlockCipher paddcbc = new PaddedBufferedBlockCipher(cbcrc6, new ZeroBytePadding());
                    paddcbc.reset();
                    paddcbc.init(false, parIV);
                    // decryption pass
                    plainText = new byte[paddcbc.getOutputSize(message.length)];
                    int ptLength = paddcbc.processBytes(message, 0, message.length, plainText, 0);
                    try {
                        ptLength += paddcbc.doFinal(plainText, ptLength);
                    } catch (Exception ex) {

                    }

                } else if (mode.equals("OFB")) {
                    OFBBlockCipher cbcrc6 = new OFBBlockCipher(blowFishEng, subblock);
                    PaddedBufferedBlockCipher paddcbc = new PaddedBufferedBlockCipher(cbcrc6, new ZeroBytePadding());
                    paddcbc.reset();
                    paddcbc.init(false, parIV);
                    // decryption pass
                    plainText = new byte[paddcbc.getOutputSize(message.length)];
                    int ptLength = paddcbc.processBytes(message, 0, message.length, plainText, 0);
                    try {
                        ptLength += paddcbc.doFinal(plainText, ptLength);
                    } catch (Exception ex) {
                    }

                } else {
                    PaddedBufferedBlockCipher paddcbc = new PaddedBufferedBlockCipher(blowFishEng, new ZeroBytePadding());
                    paddcbc.reset();
                    paddcbc.init(false, kpar);
                    // decryption pass
                    plainText = new byte[paddcbc.getOutputSize(message.length)];
                    int ptLength = paddcbc.processBytes(message, 0, message.length, plainText, 0);
                    try {
                        ptLength += paddcbc.doFinal(plainText, ptLength);
                    } catch (Exception ex) {

                    }
                }

            }

            FileOutputStream out;
            try {
                out = new FileOutputStream(plikWyjsciowy);
                out.write(plainText);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }
}


