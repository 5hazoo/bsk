package blowfish;

import org.bouncycastle.crypto.DataLengthException;
import org.bouncycastle.crypto.InvalidCipherTextException;
import org.bouncycastle.crypto.engines.BlowfishEngine;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.params.KeyParameter;

import javax.crypto.Cipher;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;


public class RSAEncryptionDescription {

    private static final String PUBLIC_KEY_FILE = "Adam";
    private static final String PRIVATE_KEY_FILE = "Adam.key";

    public static void main() throws IOException {

        try {
            System.out.println("-------GENRATE PUBLIC and PRIVATE KEY-------------");
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(1024); //1024 used for normal securities
            KeyPair keyPair = keyPairGenerator.generateKeyPair();
            PublicKey publicKey = keyPair.getPublic();
            PrivateKey privateKey = keyPair.getPrivate();
            System.out.println("Public Key - " + publicKey);
            System.out.println("Private Key - " + privateKey);

            //Pullingout parameters which makes up Key
            System.out.println("\n------- PULLING OUT PARAMETERS WHICH MAKES KEYPAIR----------\n");
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            RSAPublicKeySpec rsaPubKeySpec = keyFactory.getKeySpec(publicKey, RSAPublicKeySpec.class);
            RSAPrivateKeySpec rsaPrivKeySpec = keyFactory.getKeySpec(privateKey, RSAPrivateKeySpec.class);
            System.out.println("PubKey Modulus : " + rsaPubKeySpec.getModulus());
            System.out.println("PubKey Exponent : " + rsaPubKeySpec.getPublicExponent());
            System.out.println("PrivKey Modulus : " + rsaPrivKeySpec.getModulus());
            System.out.println("PrivKey Exponent : " + rsaPrivKeySpec.getPrivateExponent());

            //Share public key with other so they can encrypt data and decrypt thoses using private key(Don't share with Other)
            System.out.println("\n--------SAVING PUBLIC KEY AND PRIVATE KEY TO FILES-------\n");
            RSAEncryptionDescription rsaObj = new RSAEncryptionDescription();
            rsaObj.saveKeys(PUBLIC_KEY_FILE, rsaPubKeySpec.getModulus(), rsaPubKeySpec.getPublicExponent());
            rsaObj.saveKeysS(PRIVATE_KEY_FILE, rsaPrivKeySpec.getModulus(), rsaPrivKeySpec.getPrivateExponent(), "basia");

            //Encrypt Data using Public Key
            byte[] encryptedData = rsaObj.encryptData("Anuj Patel - Classified Information !".getBytes(), PUBLIC_KEY_FILE);

            //Descypt Data using Private Key
            System.out.print(rsaObj.decryptData(encryptedData, PRIVATE_KEY_FILE));


        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }

    }

    /**
     * Save Files
     *
     * @param fileName
     * @param mod
     * @param exp
     * @throws IOException
     */
    public static void saveKeys(String fileName, BigInteger mod, BigInteger exp) throws IOException {
        FileOutputStream fos = null;

        try {
            System.out.println("Generating " + mod + "...");
            fos = new FileOutputStream(fileName);
            fos.write(("<RSAKeyValue>" + '\n').getBytes());
            fos.write(("<Modulus>").getBytes());
            fos.write(DatatypeConverter.printHexBinary(mod.toByteArray()).getBytes());

            //  fos.write();
            fos.write(("</Modulus>" + '\n').getBytes());
            fos.write(("<Exponent>").getBytes());
            fos.write(DatatypeConverter.printHexBinary(exp.toByteArray()).getBytes());

            //   fos.write(exp.toByteArray());
            fos.write(("</Exponent>" + '\n').getBytes());

            fos.write(("</RSAKeyValue>" + '\n').getBytes());

            System.out.println(fileName + " generated successfully");
        } catch (Exception e) {
            e.printStackTrace();
        }

        fos.close();

    }

    public static void saveKeysS(String fileName, BigInteger mod, BigInteger exp, String pass) throws IOException, NoSuchAlgorithmException {
        FileOutputStream fos = null;

        try {
            System.out.println("Generating " + mod + "...");
            fos = new FileOutputStream(fileName);
            fos.write(("<RSAKeyValue>" + '\n').getBytes());
            fos.write(("<Modulus>").getBytes());
            fos.write(DatatypeConverter.printHexBinary(mod.toByteArray()).getBytes());

            //  fos.write();
            fos.write(("</Modulus>" + '\n').getBytes());
            fos.write(("<Exponent>").getBytes());
            fos.write(DatatypeConverter.printHexBinary(exp.toByteArray()).getBytes());

            //   fos.write(exp.toByteArray());
            fos.write(("</Exponent>" + '\n').getBytes());

            fos.write(("</RSAKeyValue>" + '\n').getBytes());

            System.out.println(fileName + " generated successfully");
            fos.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

        Path filePath = Paths.get(fileName);

        byte[] a = Files.readAllBytes(filePath);
        File file = new File(fileName);
        BlowfishEngine blowFishEng = new BlowfishEngine();

        MessageDigest md = MessageDigest.getInstance("SHA1");
        byte[] digest = md.digest(pass.getBytes());
        KeyParameter kpar = new KeyParameter(digest);

        PaddedBufferedBlockCipher paddcbc = new PaddedBufferedBlockCipher(blowFishEng);
        paddcbc.init(true, kpar);
        byte[] cipherText2 = new byte[paddcbc.getOutputSize(a.length)];
        System.out.println("Output size= " + paddcbc.getOutputSize(a.length));
        int nprocess2 = paddcbc.processBytes(a, 0, a.length, cipherText2, 0);
        try {
            nprocess2 = paddcbc.doFinal(cipherText2, nprocess2);
        } catch (DataLengthException ex) {
            ex.printStackTrace();
        } catch (InvalidCipherTextException ex) {
            ex.printStackTrace();
        } catch (IllegalStateException ex) {
            ex.printStackTrace();
        }
        FileOutputStream fis = new FileOutputStream(file.getPath());
        fis.write(cipherText2);
        fis.close();

    }


    public static byte[] encryptData(byte[] dataToEncrypt, String key) throws IOException {
        byte[] encryptedData = null;
        try {
            System.out.print(key);
            PublicKey pubKey = readPublicKeyFromFile(key);
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            encryptedData = cipher.doFinal(dataToEncrypt);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return encryptedData;
    }

    public static byte[] decryptData(byte[] data, String key) throws IOException {
        byte[] descryptedData = null;

        try {
            PrivateKey privateKey = readPrivateKeyFromFile(key);
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            descryptedData = cipher.doFinal(data);
            System.out.println("Decrypted Data: " + new String(descryptedData));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return descryptedData;
    }

    public static byte[] decryptDataS(byte[] data, String key, String pass) throws IOException {
        byte[] descryptedData = null;

        try {
            System.out.print(key + " " + pass + "\n");

            PrivateKey privateKey = readPrivateKeyFromFileS(key, pass);
            Cipher cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            descryptedData = cipher.doFinal(data);
            System.out.println("Decrypted Data: " + new String(descryptedData));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return descryptedData;
    }

    /**
     * read Public Key From File
     *
     * @param fileName
     * @return PublicKey
     * @throws IOException
     */
    public static PublicKey readPublicKeyFromFile(String fileName) throws IOException {
        FileInputStream fis = null;
        File file = new File(fileName);
        fis = new FileInputStream(file);
        XMLParser parser = new XMLParser(file);

        BigInteger modulus = new BigInteger(DatatypeConverter.parseHexBinary(new String(parser.getValue("<Modulus>", "</Modulus>", 0))));
//        BigInteger modulus = new BigInteger(parser.getValue("<Modulus>", "</Modulus>",0));
//        BigInteger exponent = new BigInteger(parser.getValue("<Exponent>", "</Exponent>",0));
        BigInteger exponent = new BigInteger(DatatypeConverter.parseHexBinary(new String(parser.getValue("<Exponent>", "</Exponent>", 0))));

//        BigInteger modulus = new BigInteger(parser.getModulus());
//        BigInteger exponent = new BigInteger(parser.getExponent());

        RSAPublicKeySpec rsaPublicKeySpec = new RSAPublicKeySpec(modulus, exponent);
        KeyFactory fact = null;
        try {
            fact = KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        PublicKey publicKey = null;
        try {
            publicKey = fact.generatePublic(rsaPublicKeySpec);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }

        return publicKey;

    }

    public static PrivateKey readPrivateKeyFromFile(String fileName) throws IOException {
        FileInputStream fis = null;
        File file = new File(fileName);
        fis = new FileInputStream(file);
        XMLParser parser = new XMLParser(file);

//        BigInteger modulus = new BigInteger(parser.getValue("<Modulus>", "</Modulus>",0));
//
//        BigInteger exponent = new BigInteger(parser.getValue("<Exponent>", "</Exponent>",0));
        BigInteger modulus = new BigInteger(DatatypeConverter.parseHexBinary(new String(parser.getValue("<Modulus>", "</Modulus>", 0))));
//        BigInteger modulus = new BigInteger(parser.getValue("<Modulus>", "</Modulus>",0));
//        BigInteger exponent = new BigInteger(parser.getValue("<Exponent>", "</Exponent>",0));
        BigInteger exponent = new BigInteger(DatatypeConverter.parseHexBinary(new String(parser.getValue("<Exponent>", "</Exponent>", 0))));
        //Get Private Key
        RSAPrivateKeySpec rsaPrivateKeySpec = new RSAPrivateKeySpec(modulus, exponent);
        KeyFactory fact = null;
        try {
            fact = KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        PrivateKey privateKey = null;
        try {
            privateKey = fact.generatePrivate(rsaPrivateKeySpec);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }

        return privateKey;
    }

    public static PrivateKey readPrivateKeyFromFileS(String fileName, String pass) throws IOException, NoSuchAlgorithmException {
        File file = new File(fileName);
        Path filePath = Paths.get(fileName);
        byte[] a = Files.readAllBytes(filePath);

        System.out.print(fileName);
        BlowfishEngine blowFishEng = new BlowfishEngine();

        MessageDigest md = MessageDigest.getInstance("SHA1");
        System.out.print(pass);
        byte[] digest = md.digest(pass.getBytes());
        KeyParameter key = new KeyParameter(digest);


        PaddedBufferedBlockCipher paddcbc = new PaddedBufferedBlockCipher(blowFishEng);
        paddcbc.reset();
        paddcbc.init(false, key);
        // decryption pass
        byte[] pt = new byte[paddcbc.getOutputSize(a.length)];
        int ptLength = paddcbc.processBytes(a, 0, a.length, pt, 0);
        try {
            ptLength += paddcbc.doFinal(pt, ptLength);
        } catch (Exception ex) {
            return null;
        }
        File f = new File("tmp");
        FileOutputStream fis = new FileOutputStream(f);
        fis.write(pt);
        fis.close();


        XMLParser parser = new XMLParser(f);

//        BigInteger modulus = new BigInteger(parser.getValue("<Modulus>", "</Modulus>",0));
//
//        BigInteger exponent = new BigInteger(parser.getValue("<Exponent>", "</Exponent>",0));
        BigInteger modulus = new BigInteger(DatatypeConverter.parseHexBinary(new String(parser.getValue("<Modulus>", "</Modulus>", 0))));
//        BigInteger modulus = new BigInteger(parser.getValue("<Modulus>", "</Modulus>",0));
//        BigInteger exponent = new BigInteger(parser.getValue("<Exponent>", "</Exponent>",0));
        BigInteger exponent = new BigInteger(DatatypeConverter.parseHexBinary(new String(parser.getValue("<Exponent>", "</Exponent>", 0))));
        //Get Private Key
        RSAPrivateKeySpec rsaPrivateKeySpec = new RSAPrivateKeySpec(modulus, exponent);
        KeyFactory fact = null;
        try {
            fact = KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        PrivateKey privateKey = null;
        try {
            privateKey = fact.generatePrivate(rsaPrivateKeySpec);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
    f.delete();
        return privateKey;
    }
}
